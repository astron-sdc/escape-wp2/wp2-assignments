## Assignment 2

### Feedback
The python bindings are pretty nice (looking into the rucio command line client code is quite enlightening). I think it would be a good idea to have a future assignment to use them.

Also I think it would be nice to have a look at non-deterministics RSEs. Those would fit our use case (LOFAR) better. 

Also I would like to investigate more how one file could for example be part of multiple data sets/containers.
