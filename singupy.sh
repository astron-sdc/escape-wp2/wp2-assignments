#!/usr/bin/env bash
singularity exec -B ${HOME}/.rucio/:/opt/rucio/etc ${HOME}/rucio_dev/rucio-py3.simg python3 $*
